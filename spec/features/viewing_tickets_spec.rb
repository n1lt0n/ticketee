require "rails_helper"

RSpec.feature "Users can view tickets" do
  before do
    author = FactoryGirl.create(:user)

    sublime = FactoryGirl.create(:project, name: "Sublime Text 3") #the factory girl creates the project with a name
    FactoryGirl.create(:ticket, project: sublime, author: author, name: "Make it shiny!",
      description: "Gradients! Starbursts! Oh my!")#FG creates a ticket under the previous project

    ie = FactoryGirl.create(:project, name: "Internet Explorer")#FG creates a project called Internet Explorer
    FactoryGirl.create(:ticket, project: ie, author: author, name: "Standards compliance",
      description: "Isn't a joke.")#FG creates a ticket under the project ie

    visit "/"

  end
  #first scenario
  scenario "for a given project" do
    click_link "Sublime Text 3"

    expect(page).to have_content "Make it shiny!"

    click_link "Make it shiny!"
    within "#ticket h2" do #how it searches for h2 without the h2 tag!?
      expect(page).to have_content "Make it shiny!"
    end

    expect(page).to have_content "Gradients! Starbursts! Oh my!"
  end
end
